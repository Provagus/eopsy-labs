#!/bin/bash
#Patryk Grzegorczyk
#Some parts of code based on https://linuxconfig.org/bash-scripting-tutorial

processFile () {
    #File name and extension extracting based on https://www.cyberciti.biz/faq/bash-get-basename-of-filename-or-directory-name/ tutorial
    DIR_NAME=`dirname "$1"`
    FILE_NAME=`basename "$1"`
    EXTENSION="${FILE_NAME#*.}" #Will be equal to filename if no extension
    if [ "X$EXTENSION" == "X$FILE_NAME" ]; then #Case for file with no extension
        EXTENSION=""
        FILE_NAME=`basename "$1"`
    elif [ "X.$EXTENSION" == "X$FILE_NAME" ]; then #Case for files starting with .
        EXTENSION=""
        FILE_NAME=`basename "$1"`
    else #Case for file with extension
        FILE_NAME=`basename "$1" ".$EXTENSION"`
    fi
    MOD_FILE_NAME=`echo "$FILE_NAME" | sed "$SED_PATTERN"`
    if [ "X$EXTENSION" == "X" ]; then #Checking if extension is empty string
        NEW_FILE="$DIR_NAME/$MOD_FILE_NAME"
    else
        NEW_FILE="$DIR_NAME/$MOD_FILE_NAME.$EXTENSION"
    fi
    mv "$1" "$1.mov.tmp.modified" #mv doesn't allow changing name, if the only change is casing
    mv "$1.mov.tmp.modified" "$NEW_FILE" #so I change name twice to make it distinguishable for mv
    #mv: './TEST_FILE.tar.gz.custom' and './test_file.tar.gz.custom' are the same file
    echo "Renamed: $1 -> $NEW_FILE"
}

recursiveMode () {
    #https://unix.stackexchange.com/questions/50692/executing-user-defined-function-in-a-find-exec-call
    #https://unix.stackexchange.com/questions/614428/executing-user-defined-function-in-a-find-exec-call-and-choosing-version-of-tha
    export -f processFile
    export SED_PATTERN
    find "$1" -type f -exec bash -c 'processFile "$@"' bash "{}" \;
}

ERROR_MESSAGE=$'Bad command usage. If you need help use modify -h'

ARGS=("$@") #Moving arguments to array
ARG_NUM=$# #Taking number of arguments

RECURSIVE=0 #parameter telling if file editing will be recursive
UPPER=0 #paramter telling if file name should be uppercased
LOWER=0 #paramter telling if file name should be lowercased

if [ $ARG_NUM -eq 0 ]; then
    echo $ERROR_MESSAGE 1>&2
    exit 1
fi

declare -a DESTINATIONS #declaring array for keeping all directories from arguments
SED_PATTERN_FOUND=0

COUNT=0
#Reading arguments with dash
for argument in $@; do
    if [ $argument = '-h' ] || [ $argument = '-help' ]; then #Printing help
        if [ $ARG_NUM -gt 1 ]; then #With help option only 1 argument (-h or -help) is allowed
            echo $ERROR_MESSAGE 1>&2
            exit 1
        fi
        echo $'Usage:
modify [-r] [-l] [-u] <dir/filename>
modify [-r] <sed pattern> <dir/file names...>
modify [-h]
modify [-help]\n
Script changing file names. Can change single file name or multiple file names in recursive mode.'
        exit 0
    elif [ $argument = '-r' ]; then
        RECURSIVE=1
    elif [ $argument = '-u' ]; then
        UPPER=1
    elif [ $argument = '-l' ]; then
        LOWER=1
    elif [ $SED_PATTERN_FOUND -eq 0 ]; then
        SED_PATTERN_FOUND=1
        SED_PATTERN="$argument"
    else
        DESTINATIONS[$COUNT]="$argument"
        ((COUNT++))
    fi
done

if [ $RECURSIVE -eq 1 ] && [ $ARG_NUM -lt 3 ]; then #if recursive option is on we need at least 3 arguments
    echo $ERROR_MESSAGE 1>&2
    exit 1
elif [ $ARG_NUM -lt 2 ] && [ $RECURSIVE -eq 0 ]; then #otherwise we need at least 2 arguments
    echo $ERROR_MESSAGE 1>&2
    exit 1
fi

if [ $UPPER -eq 1 ] && [ $LOWER -eq 1 ]; then #return error if upper and lower at once
    echo $ERROR_MESSAGE 1>&2
    exit 1
fi

if [ $UPPER -eq 1 ] || [ $LOWER -eq 1 ]; then
    #if upper or lower sed pattern argument is actually file
    DESTINATIONS[$COUNT]="$SED_PATTERN"
elif [ -e "$SED_PATTERN" ]; then
    #if we expect sed pattern then checking if sed pattern is file, if it's file then I assume user error at input
    echo "Sed pattern not inserted. If you need help use modify -h" 1>&2
    exit 1
fi

if [ $UPPER -eq 1 ]; then
    #https://stackoverflow.com/questions/4569825/sed-one-liner-to-convert-all-uppercase-to-lowercase
    SED_PATTERN='s/\(.*\)/\U\1/'
elif [ $LOWER -eq 1 ]; then
    #https://stackoverflow.com/questions/4569825/sed-one-liner-to-convert-all-uppercase-to-lowercase
    SED_PATTERN='s/\(.*\)/\L\1/'
fi

for DEST in ${DESTINATIONS[@]}; do
    if [ $RECURSIVE -eq 0 ]; then #Non recursive option
        if [ -f "$DEST" ]; then #Checking for file existence
            processFile "$DEST"
        else
            echo 'File not found. If you need help use modify -h' 1>&2
            exit 1
        fi
    else #recursive mode
        if [ -d "$DEST" ]; then
            recursiveMode "$DEST"
        elif [ -f "$DEST" ]; then
            processFile "$DEST"
        else
            echo 'Directory / file not found. If you need help use modify -h' 1>&2
            exit 1
        fi
    fi
done
