#!/bin/bash
#Patryk Grzegorczyk

touch "test_file.tar.gz.custom"
echo $'Created file: test_file.tar.gz.custom\n'
echo "Command performed: modify.sh -u \"test_file.tar.gz.custom\""
source modify.sh -u "test_file.tar.gz.custom"
mkdir -p test_directory
touch "test_directory/example"
touch "test_directory/eXaMpLe2.tar.gz"
touch "test_directory/.EXAMPLE3"
echo $'\nCreated test_directory with the following files inside:
example
eXaMpLe2.tar.gz
.EXAMPLE3\n'
echo "Command performed: modify.sh -u -r \"test_directory\""
source modify.sh -u -r "test_directory"
echo $'\nCommand performed: modify.sh -l \"test_directory/.EXAMPLE3\"'
source modify.sh -l "test_directory/.EXAMPLE3"
mkdir -p test_directory/test2
touch "test_directory/test2/Example.pdf"
echo $'\nCreated test_directory/test2 with the following files inside:
Example.pdf\n'
echo $'Command performed: modify.sh -r \'s/\\(.*\\)/\\L\\1/\' test_directory'
source modify.sh -r 's/\(.*\)/\L\1/' test_directory
echo $'\nCommand performed: modify.sh \'s/\\(.*\\)/\\U\\1/\' \"test_directory/test2/example.pdf\"'
source modify.sh 's/\(.*\)/\U\1/' "test_directory/test2/example.pdf"