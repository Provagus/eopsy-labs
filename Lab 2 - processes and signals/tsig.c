#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include<sys/wait.h>

const int NUM_CHILD = 12;
#ifdef WITH_SIGNALS
bool isInterrupted;

void childSignalHandler(int sig){
    printf("child[%i] SIGTERM signal received. Terminating\n", getpid());
    exit(0);
}

void sigintSignalHandler(int sig){
    isInterrupted = true;
    printf("parent[%i] keyboard interrupt occured\n", getpid());
    fflush(stdout);
}
#endif

int main(){
    int pid;
    int createdChilds = 0;
    pid_t childs[NUM_CHILD];
    bool isChild = false;
    #ifdef WITH_SIGNALS
    // Forcing program to ignore all signals
    for(int i = 1; i < NSIG; i++){
        signal(i, SIG_IGN);
    }
    signal(SIGCHLD, SIG_DFL);
    signal(SIGINT, sigintSignalHandler);
    #endif
    // making NUM_CHILD childs
    for(int i = 0; i < NUM_CHILD; i++){
        #ifdef WITH_SIGNALS
        if(isInterrupted && !isChild){
            printf("parent[%i] creation process interrupted\n", getpid());
            fflush(stdout);
            for(int j = 0; j < createdChilds; j++){
                kill(childs[j], SIGTERM);
            }
            break;
        }
        #endif
        // we want NUM_CHILD not 2^NUM_CHILD
        // so only parrent can continue using fork()
        // and child must leave the loop
        pid = fork();
        if(pid < 0){
            // Error occured
            fprintf(stderr, "parent[%i] Error occured while creating child\n", getpid());
            printf("parent[%i] Sending SIGTERM signal\n", getpid());
            for(int j = 0; j < i; j++){
                kill(childs[j], SIGTERM);
            }
            exit(1);
        }
        else if (pid == 0){
            // Child
            printf("child[%i] Created process with parent id: %i\n", getpid(), getppid());
            childs[i] = getpid();
            fflush(stdout);
            isChild = true;
            break;
        }
        else{
            // Parent
            createdChilds++;
            if(i + 1 != NUM_CHILD){ // if needs to create another child wait 1s
                sleep(1);
            }
        }
    }
    // child
    if(isChild){
        #ifdef WITH_SIGNALS
        // disabling signals enabled for parent only
        signal(SIGINT, SIG_IGN);
        signal(SIGCHLD, SIG_IGN);
        // custom child SIGTERM handler
        signal(SIGTERM, childSignalHandler);
        #endif
        sleep(10);
        printf("child[%i] process finished\n", getpid());
        fflush(stdout);
        exit(0);
    }
    // parent
    else{
        int i = 0;
        int exitCode;
        pid_t childPid;
        for(; i < createdChilds; i++){
            childPid = wait(&exitCode);
            printf("parent[%i] received exit code %i from child [%i]\n", getpid(), exitCode, childPid);
            fflush(stdout);
        }
        printf("parent[%i] %i childs finished their tasks\n", getpid(), i);
        #ifdef WITH_SIGNALS
        // Restoring default signal handling
        for(int i = 0; i < NSIG; i++){
            signal(i, SIG_DFL);
        }
        #endif
    }
    return 0;
}
