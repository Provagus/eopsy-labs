#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>

using namespace std;

const uint NUM_SEATS = 5;
pid_t childs[5];
int eaten[5];
bool leftHanded = false;
int semID;
key_t key = 1232456; /* key to pass to semget() */
int semflg = IPC_CREAT | 0666;
short status[5];
const short GRABBING = 0;
const short EATING = 1;
const short THINKING = 2;

void grab_forks(int left_fork_id){
    int left = left_fork_id;
    int right = ++left_fork_id % NUM_SEATS;
    struct sembuf sops[2];
    struct sembuf sops2[2];

    if(leftHanded){
        //grab left fork
        sops[0].sem_num = left;
        sops[0].sem_op = 0;
        sops[0].sem_flg = SEM_UNDO;

        sops[1].sem_num = left;
        sops[1].sem_op = 1;
        sops[1].sem_flg = SEM_UNDO;

        //grab right fork
        sops2[0].sem_num = right;
        sops2[0].sem_op = 0;
        sops2[0].sem_flg = SEM_UNDO;

        sops2[1].sem_num = right;
        sops2[1].sem_op = 1;
        sops2[1].sem_flg = SEM_UNDO;

        if (semop(semID, sops, 2) == -1) {
            cerr << "Semaphore error\n";
            exit(EXIT_FAILURE);
        }
        sleep(1);
        if (semop(semID, sops2, 2) == -1) {
            cerr << "Semaphore error\n";
            exit(EXIT_FAILURE);
        }
    }
    else{
        //grab right fork
        sops[0].sem_num = right;
        sops[0].sem_op = 0;
        sops[0].sem_flg = SEM_UNDO;

        sops[1].sem_num = right;
        sops[1].sem_op = 1;
        sops[1].sem_flg = SEM_UNDO;

        //grab left fork
        sops2[0].sem_num = left;
        sops2[0].sem_op = 0;
        sops2[0].sem_flg = SEM_UNDO;

        sops2[1].sem_num = left;
        sops2[1].sem_op = 1;
        sops2[1].sem_flg = SEM_UNDO;

        if (semop(semID, sops, 2) == -1) {
            cerr << "Semaphore error\n";
            exit(EXIT_FAILURE);
        }
        sleep(1);
        if (semop(semID, sops2, 2) == -1) {
            cerr << "Semaphore error\n";
            exit(EXIT_FAILURE);
        }
    }
}

void put_away_forks(int left_fork_id){
    int left = left_fork_id;
    int right = ++left_fork_id % NUM_SEATS;
    struct sembuf sops[2];
    sops[0].sem_num = left;
    sops[0].sem_op = -1;
    sops[0].sem_flg = SEM_UNDO;

    sops[1].sem_num = right;
    sops[1].sem_op = -1;
    sops[1].sem_flg = SEM_UNDO;

    if (semop(semID, sops, 2) == -1) {
        cerr << "Semaphore error\n";
        exit(EXIT_FAILURE);
    }
}

void PrintStatus(){
    int retValue;
    /*#if defined _WIN32
        retValue = system("cls");
    #elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
        retValue = system("clear");
    #elif defined (__APPLE__)
        retValue = system("clear");
    #endif*/
    cout << "=====================================\n\n";
    for(int i = 0; i < NUM_SEATS; i++){
        switch(status[i]){
            case GRABBING:
                cout << "G";
                break;
            case EATING:
                cout << "E";
                break;
            case THINKING:
                cout << "T";
                break;
        }
        cout << " ";
    }
    cout << "\n\nG - Grabbing forks\nE - Eating\nT - Thinking\n\n";
    for(int i = 0; i < NUM_SEATS; i++){
        switch(status[i]){
            case GRABBING:
                cout << "Philosopher " << i << " is grabbing forks\n";
                break;
            case EATING:
                cout << "Philosopher " << i << " is eating\n";
                break;
            case THINKING:
                cout << "Philosopher " << i << " is thinking\n";
                break;
        }
        cout << "Philosopher ate " << eaten[i] << " times\n";
    }
    cout << flush;
}

void SignalHanler(int sig){
    int philosopher = sig - SIGRTMIN;
    status[philosopher]++;
    status[philosopher] %= 3;
    if(status[philosopher] == THINKING){
        eaten[philosopher]++;
    }
    PrintStatus();
}

int main(){
    if ((semID = semget(key, NUM_SEATS, semflg)) == -1) {
        cerr << "semget: semget failed\n";
        exit(EXIT_FAILURE);
    }
    for(uint i = 0; i < NUM_SEATS; i++){
        signal(SIGRTMIN + i, SignalHanler);
    }
    int pid;
    bool isChild = false;
    int philosopherNumber = 0;
    // Creating 5 philosophers
    for(uint i = 0; i < NUM_SEATS; i++){
        pid = fork();
        if(pid < 0){
            cerr << "Error occured during creation of philosophers\n";
            for(int j = 0; j < i; j++){
                kill(childs[j], SIGTERM);
            }
            exit(1);
        }
        else if (pid == 0){
            // Child
            philosopherNumber = i; //assigning philosopher number
            leftHanded = i % 2; //assigning if is left handed
            isChild = true;
            break;
        }
    }
    if(isChild){
        srand(time(NULL)+philosopherNumber);
        while(true){
            grab_forks(philosopherNumber); //grabbing forks
            kill(getppid(), SIGRTMIN + philosopherNumber);
            //sleep(rand() % 12 + 1);
            sleep(2);
            put_away_forks(philosopherNumber); //put away forks
            kill(getppid(), SIGRTMIN + philosopherNumber);
            //sleep(rand() % 12 + 1);
            sleep(2);
            kill(getppid(), SIGRTMIN + philosopherNumber);
        }
    }
    else{
        //Parent
        PrintStatus();
        for(uint i = 0; i < NUM_SEATS; i++){
            wait(NULL);
        }
    }
    return 0;
}