#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 100
#define EXIT_FAILURE 1

const char HELP_TEXT[] = "Usage: copy [-m] <file name> <new file name>\nm parameter is used to forbid using read neither write\ncopy [-h] to get help\n";
const char HELP_PARAMETER[] = "-h";
const char M_PARAMETER[] = "-m";
const char ERROR_INPUT[] = "Provided input is invalid. Use copy -h to see help message\n";

char buf[BUF_SIZE];

void copy_read_write(int fd_from, int fd_to){
    ssize_t readSize, writeSize;
    do{ //loop as long as file has something to copy left
        readSize = read(fd_from, buf, sizeof(buf)); //reading part of file to buffor
        if(readSize <= 0){
            break;
        }
        writeSize = write(fd_to, buf, readSize); //writing to file from buffor
        if(writeSize <= 0){
            break;
        }
    }while(readSize > 0);
}

void copy_mmap(int fd_from, int fd_to){
    struct stat fileStat;
    int result;
    //reading source file informations
    result = fstat(fd_from, &fileStat);
    if(result == -1){
        fprintf(stderr, "Error occured while accessing input file\n");
        exit(EXIT_FAILURE);
    }
    char *addrFrom, *addrTo;
    //mmap source file
    addrFrom = mmap(NULL, fileStat.st_size, PROT_READ, MAP_PRIVATE, fd_from, 0);
    if(addrFrom == MAP_FAILED){
        fprintf(stderr, "Error occured while mapping from file\n");
        exit(EXIT_FAILURE);
    }
    result = ftruncate(fd_to, fileStat.st_size); //changing file size
    if(result == -1){
        fprintf(stderr, "Ftruncate error\n");
        exit(EXIT_FAILURE);
    }
    //mmap destination file
    addrTo = mmap(NULL, fileStat.st_size, PROT_WRITE | PROT_READ, MAP_SHARED, fd_to, 0);
    if(addrTo == MAP_FAILED){
        fprintf(stderr, "Error occured while mapping to file\n");
        exit(EXIT_FAILURE);
    }
    memcpy(addrTo, addrFrom, fileStat.st_size); //copying file content
    //Deallocating memory
    munmap(addrFrom, fileStat.st_size);
    munmap(addrTo, fileStat.st_size);
}

int main(int argc, char *argv[]){
    if(argc == 1){
        printf(HELP_TEXT);
        exit(0);
    }
    int readWriteAllowed = 1;
    int opt;
    int fileFrom, fileTo;
    //Reading arguments
    while((opt = getopt(argc, argv, "hm")) != -1){
        switch(opt){
            case 'h': //h option
                printf(HELP_TEXT);
                exit(0);
            case 'm': //m option
                readWriteAllowed = 0;
                break;
            default:
                fprintf(stderr, ERROR_INPUT);
                exit(EXIT_FAILURE);
        }
    }
    //Checking if amount of arguments is correct
    if(argc < 3 || (!readWriteAllowed && argc == 3) ||
        (readWriteAllowed && argc > 3) || (!readWriteAllowed && argc > 4)){
        fprintf(stderr, ERROR_INPUT);
        exit(EXIT_FAILURE);
    }
    char *oldFileName, *newFileName;
    oldFileName = argv[optind]; //Reading file names
    newFileName = argv[optind+1];
    
    //Opening file
    fileFrom = open(oldFileName, O_RDONLY);
    if(fileFrom < 0){
        fprintf(stderr, "Couldn't open input file\n");
        exit(EXIT_FAILURE);
    }
    fileTo = open(newFileName, O_RDWR | O_CREAT | O_TRUNC, 0755);
    if(fileTo < 0){
        fprintf(stderr, "Couldn't open output file\n");
        exit(EXIT_FAILURE);
    }

    if(readWriteAllowed){
        //Read write option
        copy_read_write(fileFrom, fileTo);
    }
    else{
        //Copying using mmap
        copy_mmap(fileFrom, fileTo);
    }
    if(close(fileFrom) == -1){
        fprintf(stderr, "Couldn't close inut file\n");
        exit(EXIT_FAILURE);
    }
    if(close(fileTo) == -1){
        fprintf(stderr, "Couldn't close output file\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}
