#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include<sys/wait.h>

#define N	5
#define LEFT	( i + N - 1 ) % N
#define RIGHT	( i + 1 ) % N

#define THINKING 0
#define HUNGRY 1
#define EATING 2

pthread_mutex_t	m;		//initialized to 1
int	state[N];	//initiated to THINKING's
pthread_mutex_t	s[N];		//initialized to 0's

pthread_t threads[N];

int timesEated[N];

void test(int i);

void grab_forks(int i){
	pthread_mutex_lock( &m );
		state[i] = HUNGRY;
		test( i );
	pthread_mutex_unlock(&m);
	//wait for test to unlock mutex to use it
	pthread_mutex_lock(&s[i]);
}

void put_away_forks(int i){
	pthread_mutex_lock(&m);
		state[i] = THINKING;
		test( LEFT );
		test( RIGHT );
	pthread_mutex_unlock(&m);
}

void test(int i){
	if( state[i] == HUNGRY
		&& state[LEFT] != EATING
		&& state[RIGHT] != EATING )
	{
		state[i] = EATING;
		pthread_mutex_unlock(&s[i]);
	}
}

//thread function
void *PhilosopherBehaviour(void *i){
	int philosopherNumber = *((int *)i);
	//initialising mutex for philosopher
	pthread_mutex_init(&s[philosopherNumber], NULL);
	pthread_mutex_lock(&s[philosopherNumber]);
	free(i); //freeing up pointer
	printf("Philosopher %i created\n", philosopherNumber);
	while(1){
		grab_forks(philosopherNumber);
		timesEated[philosopherNumber]++;
		sleep(1);
		//print details about how many times each philosopher ate
		printf("Philosophers ate ");
		for(int i = 0; i < N; i++){
			printf("%i ", timesEated[i]);
		}
		printf("times\n");
		put_away_forks(philosopherNumber);
		sleep(1);
	}
}

void handlerSigInt(int sig){
	//terminate threads and finish
	for(int i = 0; i < N; i++){
		pthread_kill(threads[i], SIGTERM);
		pthread_join(threads[i], NULL);
	}
	exit(EXIT_SUCCESS);
}

int main(){
	//initialising mutex m
	pthread_mutex_init(&m, NULL);

	signal(SIGINT, handlerSigInt);
	for(int i = 0; i < N; i++){
		//allocating memory for pointer to pass it
		int *tmp = malloc(sizeof(*tmp));
		if(tmp == NULL){
			exit(EXIT_FAILURE);
		}
		*tmp = i;
		pthread_create(&threads[i], NULL, PhilosopherBehaviour, (void *)tmp);
	}
	//wait for processes to finish
	for(int i = 0; i < N; i++){
		pthread_join(threads[i], NULL);
	}
	//finish
	return 0;
}

/*1. Would it be sufficient just to add to the old algorithm from task5
additional mutex variable to organize critical sections in functions
grab_forks() and put_away_forks() for making changes to values of two mutexes
indivisably?  If not, why?

In case of my particular algorithm that would be enough. Since my philosophers were left and right handed, 
so they couldn't lock each other. Tho in case of 2 mutexes and all philosophers right handed that wouldn't be enough
since philosophers could lock mutexes and not unlock them

2. Why m mutex is initialized with 1 and mutexes from the array s are
initialized with 0's?

Mutex m is initialized with 1 because we want to have it unlocked. Then philosophers can lock m and therefore
only one philosopher will try to grab forks in one certain moment. s array is initialized as locked, because we want
philosopher to wait for forks to became available (s to unlock), these mutexes can be unlocked by test function
*/
